CREATE TABLE `customers` (
    `name` VARCHAR ,
    `age` INTEGER NOT NULL ,
    `mobile_phone` BOOLEAN NOT NULL ,
    `mobile_phone_id` VARCHAR ,
    PRIMARY KEY (`name`)
);

INSERT INTO "customers" VALUES('Feri',25,1,'Expensive');
INSERT INTO "customers" VALUES('Marta',18,1,'Expensive');

CREATE TABLE `mobile_phone` (
    `type` VARCHAR ,
    `price` INTEGER NOT NULL ,
    PRIMARY KEY (`type`)
);

INSERT INTO "mobile_phone" VALUES('Cheap',20);
INSERT INTO "mobile_phone" VALUES('Expensive',200);
