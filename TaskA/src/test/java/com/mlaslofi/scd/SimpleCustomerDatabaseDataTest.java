package com.mlaslofi.scd;

import java.sql.SQLException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.j256.ormlite.table.TableUtils;

/**
 *
 * @author mlaslofi
 */
public class SimpleCustomerDatabaseDataTest {
        
    private static SimpleCustomerDatabase scd = null;
    private static final String dbPath = "/Users/mlaslofi/code/task-a/db/test.db";
    private int customerCount;
    private int mobilePhoneCount;
    
    @BeforeClass
    public static void setup() throws Exception {
        scd = new SimpleCustomerDatabase();
        scd.setupDatabase(dbPath);
    }

    @AfterClass
    public static void teardown() throws SQLException {
        TableUtils.dropTable(scd.getConnectionSource(), Customer.class, true);
        TableUtils.dropTable(scd.getConnectionSource(), MobilePhone.class, true);
        scd.close();
    }
    
    @Before
    public void fillDb() throws SQLException {
        MobilePhone[] phones = {
            new MobilePhone("Cheap", 20),
            new MobilePhone("Expensive", 200)
        };
        
        mobilePhoneCount = phones.length;
                
        for (MobilePhone phone : phones) {
            scd.getMobilePhoneDao().create(phone);
        }
       
        Customer[] customers = {
            CustomerBuilder.build("Thomas", 18),
            CustomerBuilder.build("Minho", 18, new MobilePhone("Cheap", 20)),
            CustomerBuilder.build("Newt", 20, new MobilePhone("Expensive", 200)),
            CustomerBuilder.build("Alby", 22, new MobilePhone("Expensive", 200)),
            CustomerBuilder.build("Teresa", 19, new MobilePhone("Cheap", 20)),
            CustomerBuilder.build("Rat Man", 63, new MobilePhone("Cheap", 20)),
            CustomerBuilder.build("Chancellor Paige", 60, new MobilePhone("Expensive", 200))
        };
        
        customerCount = customers.length;
        
        for (Customer customer : customers) {
            scd.getCustomerDao().create(customer);
        }
    }
    
    @After
    public void cleanDb() throws SQLException {
        TableUtils.clearTable(scd.getConnectionSource(), Customer.class);
        TableUtils.clearTable(scd.getConnectionSource(), MobilePhone.class);
    }
    
    @Test
    public void canGetAllCustomers() throws SQLException {
        List<Customer> customers;
        customers = scd.getAllCustomers();
        assertFalse(customers.isEmpty());
        assertEquals(customerCount, customers.size());
    }

    @Test
    public void canGetEmptyListWhenNoCustomer() throws SQLException {
        List<Customer> customers;
        TableUtils.clearTable(scd.getConnectionSource(), Customer.class);

        customers = scd.getAllCustomers();
        assertTrue(customers.isEmpty());
        assertNotNull(customers);
    }
    
    @Test
    public void canGetMobilePhones() throws SQLException {
        List<MobilePhone> phones;
        phones = scd.getAllMobilePhones();
        assertFalse(phones.isEmpty());
        assertEquals(mobilePhoneCount, phones.size());
    }
    
    @Test
    public void canAddCustomer() throws SQLException {
        MobilePhone phone = new MobilePhone("Cheap", 20);
        Customer customer = CustomerBuilder.build("Brenda", 25, phone);
        
        int rowsAffected = scd.addCustomer(customer);
        assertEquals(1, rowsAffected);
        
        Customer pCustomer = CustomerBuilder.build(
                scd.getCustomerDao().queryForId(customer.getName()));
        
        assertEquals(customer, pCustomer);
        assertEquals(phone, pCustomer.getCurrentPhone());
    }
    
    @Test
    public void canGetCustomerById() throws SQLException {
        Customer alby = scd.getCustomer("Alby");
        assertNotNull(alby);
        
        Customer fake = scd.getCustomer("Fake");
        assertNull(fake);
    }
    
    @Test
    public void canDeleteCustomerById() throws SQLException {
        int rowsAffected;
        rowsAffected = scd.deleteCustomer("Thomas");
        assertEquals(1, rowsAffected);
        assertNull(scd.getCustomer("Thomas"));
    }

    @Test
    public void canOfferPhone() throws SQLException {
        Customer thomas = scd.getCustomer("Thomas");
        assertNotNull(thomas);
        assertFalse(thomas.getMobilePhone());

        thomas.offerPhone();
        int rowsAffected = scd.updateCustomer(thomas);
        assertEquals(1, rowsAffected);

        Customer pThomas = scd.getCustomer("Thomas");
        assertTrue(thomas.getMobilePhone());

    }
}
