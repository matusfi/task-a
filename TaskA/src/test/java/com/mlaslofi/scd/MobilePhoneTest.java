package com.mlaslofi.scd;

import static org.junit.Assert.*;
//import static org.hamcrest.CoreMatchers.*;
//import static org.junit.matchers.JUnitMatchers.*;

import org.junit.Test;
//import org.junit.Ignore;
//import org.hamcrest.core.CombinableMatcher;

/**
 *
 * @author mlaslofi
 */
public class MobilePhoneTest {

    @Test
    public void canCreatePhone() {
        String type = "Cheap";
        int price = 20;

        MobilePhone phone = new MobilePhone(type, price);

        assertEquals(type, phone.getType());
        assertEquals(price, phone.getPrice());
    }
}
