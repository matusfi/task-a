package com.mlaslofi.scd;

import org.junit.Test;
import org.junit.BeforeClass;
import org.junit.AfterClass;
import static org.junit.Assert.*;

import java.sql.SQLException;

public class SimpleCustomerDatabaseTest {
    
    private static SimpleCustomerDatabase scd = null;
    private static final String dbPath = "/Users/mlaslofi/code/task-a/db/test.db";
    
    @BeforeClass
    public static void setup() throws Exception {
        scd = new SimpleCustomerDatabase();
        scd.setupDatabase(dbPath);
    }

    @AfterClass
    public static void teardown() throws SQLException {
        scd.close();
    }
    
    @Test
    public void canSetUpDatabase() throws Exception {     
        assertNotNull(scd);
    }
}
