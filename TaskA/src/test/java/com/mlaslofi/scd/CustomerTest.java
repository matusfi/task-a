package com.mlaslofi.scd;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mlaslofi
 */
public class CustomerTest {

    @Test
    public void createCustomerWOPhone() {
        String name = "John Newman";
        int age = 64;
        Customer john = CustomerBuilder.build(name, age);

        assertEquals(name, john.getName());
        assertEquals(age, john.getAge());
        assertFalse(john.getMobilePhone());
        assertNull(john.getCurrentPhone());
    }

    @Test
    public void createCustomerWithPhone() {
        String name = "Bill Murray";
        int age = 72;

        String phoneType = "Cheap";
        int phonePrice = 20;

        Customer john = CustomerBuilder.build(name, age, new MobilePhone(phoneType, phonePrice));

        assertEquals(name, john.getName());
        assertEquals(age, john.getAge());
        assertTrue(john.getMobilePhone());

        MobilePhone phone = john.getCurrentPhone();

        assertNotNull(phone);
    }

    @Test
    public void whatItIs() {
        Customer x = CustomerBuilder.build("Ferko", 14);
        assertEquals(Child.class, x.getClass());
    }
}
