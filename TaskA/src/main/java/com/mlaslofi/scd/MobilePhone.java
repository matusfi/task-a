package com.mlaslofi.scd;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Yes. A mobile phone. Lousy implementation.
 *
 * @author Matus Laslofi
 * @version 0.1.0
 */
@DatabaseTable(tableName = "mobile_phone")
public class MobilePhone {

    /*
     Constants
     */
    public static final String TYPE_FIELD_NAME = "type";
    public static final String PRICE_FIELD_NAME = "price";

    /*
     Fields
     */
    @DatabaseField(id = true, columnName = TYPE_FIELD_NAME)
    private String type;

    @DatabaseField(canBeNull = false, columnName = PRICE_FIELD_NAME)
    private int price;

    /*
     Getters & Setters
     */
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * No-argument constructor is required for ORMLite.
     */
    public MobilePhone() {}

    public MobilePhone(String type, int price) {
        this.type = type;
        this.price = price;
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || other.getClass() != getClass()) {
            return false;
        }
        return type.equals(((MobilePhone) other).type);
    }


}
