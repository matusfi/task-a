package com.mlaslofi.scd;

/**
 * CustomerBuilder allows you to build the right type of Customer, depending on
 * the Customer's age.
 *
 * @see Customer
 * @see Child
 * @see Adult
 * @see Elder
 * @author mlaslofi
 * @version 0.1.0
 */
public class CustomerBuilder {

    private static final int childThreshold = 17;
    private static final int adultThreshold = 60;


    public static Customer build(String name, int age) {
        if (age <= childThreshold) {
            return new Child(name, age);
        } else if (age <= adultThreshold) {
            return new Adult(name, age);
        } else {
            return new Elder(name, age);
        }
    }

    public static Customer build(String name, int age, MobilePhone phone) {
        if (age <= childThreshold) {
            return new Child(name, age, phone);
        } else if (age <= adultThreshold) {
            return new Adult(name, age, phone);
        } else {
            return new Elder(name, age, phone);
        }
    }

    public static Customer build(Customer customer) {
        if (customer != null) {
            String name = customer.getName();
            int age = customer.getAge();
            MobilePhone phone = customer.getCurrentPhone();

            if (age <= childThreshold) {
                return new Child(name, age, phone);
            } else if (age <= adultThreshold) {
                return new Adult(name, age, phone);
            } else {
                return new Elder(name, age, phone);
            }
        } else {
            return null;
        }
    }
}
