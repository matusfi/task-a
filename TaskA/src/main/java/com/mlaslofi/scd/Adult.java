package com.mlaslofi.scd;

/**
 * Adult version of a Customer. Depending on settings in CustomerBuilder, it
 * should be only Customer between 18 and 60 years old, inclusive.
 *
 * @see CustomerBuilder
 * @author mlaslofi
 */
public class Adult extends Customer {

    public Adult(String name, int age) {
        super(name, age);
    }

    public Adult(String name, int age, MobilePhone phone) {
        super(name, age, phone);
    }

    @Override
    public MobilePhone offerPhone() {
        MobilePhone phone = new MobilePhone("Expensive", 200);
        this.setCurrentPhone(phone);
        return phone;
    }
}
