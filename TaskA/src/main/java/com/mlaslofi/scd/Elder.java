package com.mlaslofi.scd;

/**
 * Elder version of a Customer. Depending on settings in CustomerBuilder, it
 * should be only Customer over 60 years old.
 *
 * @see CustomerBuilder
 * @author mlaslofi
 */
public class Elder extends Customer {

    public Elder(String name, int age) {
        super(name, age);
    }

    public Elder(String name, int age, MobilePhone phone) {
        super(name, age, phone);
    }

    @Override
    public MobilePhone offerPhone() {
        MobilePhone phone = new MobilePhone("Cheap", 20);
        this.setCurrentPhone(phone);
        return phone;
    }
}
