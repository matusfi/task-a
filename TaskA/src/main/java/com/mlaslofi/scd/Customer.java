package com.mlaslofi.scd;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * General Customer class.
  *
 * @author Matus Laslofi
 * @version 0.1.0
 * @see CustomerInterface
 * @see CustomerBuilder
 * @see Adult
 * @see Child
 * @see Elder
 */
@DatabaseTable(tableName = "customers")
public class Customer implements CustomerInterface {

    /*
     Constants
     */
    public static final String NAME_FIELD_NAME = "name";
    public static final String AGE_FIELD_NAME = "age";
    public static final String MOBILE_PHONE_FIELD_NAME = "mobile_phone";
    public static final String CURRENT_MOBILE_PHONE_FIELD_NAME = "mobile_phone_id";

    /*
     Fields
     */
    @DatabaseField(id = true, columnName = NAME_FIELD_NAME)
    private String name;

    @DatabaseField(canBeNull = false, columnName = AGE_FIELD_NAME)
    private int age;

    @DatabaseField(canBeNull = false, columnName = MOBILE_PHONE_FIELD_NAME)
    private boolean mobilePhone;

    @DatabaseField(foreign = true, columnName = CURRENT_MOBILE_PHONE_FIELD_NAME)
    private MobilePhone currentPhone;

    /*
     Getters and Setters
     */
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(boolean mp) {
        this.mobilePhone = mp;
    }

    public MobilePhone getCurrentPhone() {
        return currentPhone;
    }

    /**
     * When you assign a phone to customer, it automatically turns mobilePhone
     * field on.
     *
     * @param mp
     */
    public void setCurrentPhone(MobilePhone mp) {
        this.currentPhone = mp;
        this.mobilePhone = (mp != null);
    }

    Customer() {
        // for ORMLite
    }

    public Customer(String name, int age) {
        this.name = name;
        this.age = age;
        this.setCurrentPhone(null);
    }

    public Customer(String name, int age, MobilePhone currentPhone) {
        this.name = name;
        this.age = age;
        this.setCurrentPhone(currentPhone);
    }

    /**
     * This is mess. And only because I couldn't import Gson lib.
     *
     * @return the Customer serialized to JSON String
     */
    public String toJson() {
        String json;

        json = "{\"name\": \"" + name + "\", \"age\": "
                + age + ", \"mobilePhone\": ";

        if (mobilePhone) {
            json += "true, \"currentPhone\": {\"type\":\""
                    + currentPhone.getType()
                    + "\", \"price\": "
                    + currentPhone.getPrice()
                    + "}";
        } else {
            json += "false";
        }
        json += "}";

        return json;
    }

    @Override
    public String getInfo() {
        return toJson();
    }

    /**
     * Default implementation is offering nothing (null). Subclasses implement
     * their own offers. Due to specification, it also sets the phone. Sorry, no
     * choosing.
     *
     * @see CustomerInterface
     * @see Child
     * @see Adult
     * @see Elder
     * @return an offered MobilePhone
     */
    @Override
    public MobilePhone offerPhone() {
        this.setCurrentPhone(null);
        return null;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == null || other.getClass() != getClass()) {
            return false;
        }
        return name.equals(((Customer) other).getName());
    }
}
