package com.mlaslofi.scd;

import java.sql.SQLException;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import java.util.ArrayList;

/**
 * The database offers simple means to manipulate with Customers and
 * MobilePhones. It uses sqlite as a back-end.
 *
 * @author Matus Laslofi
 * @version 0.1.0
 */
class SimpleCustomerDatabase {

    /*
     Fields
     */
    private Dao<Customer, String> customerDao;
    private Dao<MobilePhone, String> mobilePhoneDao;
    private ConnectionSource connectionSource;

    /*
     Getters & Setters
     */
    public Dao<Customer, String> getCustomerDao() {
        return customerDao;
    }

    public void setCustomerDao(Dao<Customer, String> customerDao) {
        this.customerDao = customerDao;
    }

    public Dao<MobilePhone, String> getMobilePhoneDao() {
        return mobilePhoneDao;
    }

    public void setMobilePhoneDao(Dao<MobilePhone, String> mobilePhoneDao) {
        this.mobilePhoneDao = mobilePhoneDao;
    }

    public ConnectionSource getConnectionSource() {
        return connectionSource;
    }

    public void setConnectionSource(ConnectionSource connectionSource) {
        this.connectionSource = connectionSource;
    }


    /**
     * Setup the application database and DAOs. Create tables if they don't
     * exist yet.
     *
     * @param dbPath the path to Sqlite database file
     */
    public void setupDatabase(String dbPath) throws Exception {

        String databaseUrl = "jdbc:sqlite:" + dbPath;
        setConnectionSource(new JdbcConnectionSource(databaseUrl));

        setCustomerDao(DaoManager.createDao(getConnectionSource(), Customer.class));
        setMobilePhoneDao(DaoManager.createDao(getConnectionSource(), MobilePhone.class));

        TableUtils.createTableIfNotExists(getConnectionSource(), Customer.class);
        TableUtils.createTableIfNotExists(getConnectionSource(), MobilePhone.class);
        
    }

    /**
     * Close the connection to the database.
     *
     * @throws SQLException
     */
    public void close() throws SQLException {
        getConnectionSource().close();
    }

    /**
     * Query the database for all customers. Convert them to their right
     * subclasses.
     *
     * @see Child
     * @see Adult
     * @see Elder
     * @see CustomerBuilder
     * @return the list of customer subclasses
     * @throws SQLException
     */
    public List<Customer> getAllCustomers() throws SQLException {
        List<Customer> customers = new ArrayList<>();

        for (Customer customer : getCustomerDao()) {
            customers.add(CustomerBuilder.build(customer));
        }

        return customers;
    }

    /**
     * Query for all mobile phones.
     *
     * @return the list of all phones in the database
     * @throws SQLException
     */
    public List<MobilePhone> getAllMobilePhones() throws SQLException {
        return getMobilePhoneDao().queryForAll();
    }

    /**
     * Persist a Customer to database.
     *
     * @param customer to be persisted
     * @return the number of affected rows
     * @throws SQLException
     */
    public int addCustomer(Customer customer) throws SQLException {
        return getCustomerDao().create(customer);
    }

    /**
     * Query for a Customer by their name (currently the primary key).
     *
     * @param customerName the name of the Customer (it's the primary key)
     * @return the customer converted to right Customer subclass
     * @throws SQLException
     */
    public Customer getCustomer(String customerName) throws SQLException {
        return CustomerBuilder.build(getCustomerDao().queryForId(customerName));
    }

    /**
     * Get rid of a Customer.
     *
     * @param customerName the of the angry Customer.
     * @return the number of affected rows. Should be 1, normally.
     * @throws SQLException
     */
    public int deleteCustomer(String customerName) throws SQLException {
        return getCustomerDao().deleteById(customerName);
    }

    /**
     * Update the data of a Customer. This doesn't work on the name field, since
     * it is a Primary Key.
     *
     * @param customer the customer object that the changes were made to
     * @return the number of affected rows. Should be 1, normally.
     * @throws SQLException
     */
    public int updateCustomer(Customer customer) throws SQLException {
        return (customer != null) ? getCustomerDao().update(customer) : -1;
    }
}
