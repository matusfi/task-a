package com.mlaslofi.scd;

/**
 * Child version of Customer. Range from 0 to 17.
 *
 * @see CustomerBuilder
 * @author mlaslofi
 */
public class Child extends Customer {

    public Child(String name, int age) {
        super(name, age);
    }

    public Child(String name, int age, MobilePhone phone) {
        super(name, age, phone);
    }
}
