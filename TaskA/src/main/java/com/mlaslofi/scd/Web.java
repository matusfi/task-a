package com.mlaslofi.scd;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static spark.Spark.*;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.template.mustache.MustacheTemplateEngine;

/**
 * This is basic Spark web application. Not REST-ful at all.
 *
 * @author mlaslofi
 * @version 0.1.0
 */
public class Web {

    private static final Logger logger = Logger.getLogger(Web.class.getName());
    private static final String dbPath = "/Users/mlaslofi/code/task-a/db/sc.db";

    public static void main(String[] args) throws Exception {
        Map map = new HashMap();
        staticFileLocation("/public");

        SimpleCustomerDatabase scd = new SimpleCustomerDatabase();
        scd.setupDatabase(dbPath);
        
        /**
         * This is the basic request that gets all customers. It renders HTML
         * from a Mustache template.
         */
        get("/", (Request rq, Response rs) -> {
            try {
                map.put("customers", scd.getAllCustomers());
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, null, ex);
                halt(505);
            }
            return new ModelAndView(map, "layout.html");
        }, new MustacheTemplateEngine());

        /**
         * You can add a new customer by sending POST request with name and age
         * parameters.
         *
         * Basic validation. Needs work.
         */
        post("/customer/add", (Request rq, Response rs) -> {
            String name = rq.queryParams("name");
            int age = Integer.parseInt(rq.queryParams("age"));

            if (null == name || name.isEmpty() || age == 0) {
                logger.severe("Parameters are off.");
            } else {
                Customer customer = CustomerBuilder.build(name, age);
                try {
                    scd.addCustomer(customer);
                } catch (SQLException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }

            rs.redirect("/");
            return null;
        });

        /**
         * Delete a customer by POSTing here. The name parameter is enough this
         * time.
         */
        post("/customer/delete", (Request rq, Response rs) -> {
            String name = rq.queryParams("name");

            if (null == name || name.isEmpty()) {
                logger.severe("Parameters are off.");
            } else {
                try {
                    scd.deleteCustomer(name);
                } catch (SQLException ex) {
                    logger.log(Level.SEVERE, null, ex);
                }
            }

            rs.redirect("/");
            return null;
        });

        /**
         * Get information about a Customer and offer them (make them take) a
         * phone.
         */
        get("/customer/:name", "application/json", (Request rq, Response rs) -> {
            Customer customer = null;

            try {
                customer = scd.getCustomer(rq.params(":name"));

                if (customer != null) {
                    MobilePhone oldPhone = customer.getCurrentPhone();
                    MobilePhone newPhone = customer.offerPhone();

                    if ((oldPhone == null && newPhone != null)
                            || (oldPhone != null && !oldPhone.equals(newPhone))) {
                        scd.updateCustomer(customer);
                    }
                }
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, null, ex);
            }
            
            return (customer == null) ? "" : customer.toJson();
        });

        scd.close();
    }
}
