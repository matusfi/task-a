package com.mlaslofi.scd;

/**
 * Interface for Customers. Every customer must be able to be offered (sic!) a
 * phone, and we must be able to get information about Customer too.
 *
 * @author mlaslofi
 * @version 0.1.0
 */
public interface CustomerInterface {

    public MobilePhone offerPhone();

    public String getInfo();

}
