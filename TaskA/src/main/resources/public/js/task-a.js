/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    $("#close-flash").click(function () {
        $(".flash").hide();
    });

    $(".customer-row").hover(
            function () {
                $(this).addClass("hover-row");
            }, function () {
        $(this).removeClass("hover-row");
    });

    $(".customer-row").click(function (event) {
        var customerName = $(this).children("td.name-td").html();
        $.getJSON('/customer/' + customerName, function (json) {
            $("#ci-name").html(json.name);
            $("#ci-age").html(json.age);

            $("#name-input").val(json.name);
            $("#age-input").val(json.age);

            if (json.mobilePhone && json.currentPhone !== undefined) {
                $("#ci-phone-type")
                        .html(json.currentPhone.type + " "
                                + json.currentPhone.price + "€");
            } else {
                $("#ci-phone-type").html("no phone");
            }
            $("#customer-info").show();
        });
    });

    $("#add-button").click(function (event) {
        console.log($(this));

        var name = $("#name-input").val();
        var age = $("#age-input").val();

        if (validate(name) && validate(age)) {
            var form = $("#form");
            form.attr("action", "customer/add");
        }
    });

    $("#delete-button").click(function (event) {
        $("#age-input").attr("required", false);
        console.log($(this));

        var name = $("#name-input").val();

        if (validate(name)) {
            var form = $("#form");
            form.attr("action", "customer/delete");
        }
    });

    function validate(val) {
        return (val !== undefined && val !== null && val.length > 0) ? true : false;
    }

    function validateAge(val) {
        if (validate(val) && $(val).isNumeric() && val > 0 && val < 100)
            return true;
        else
            return false;
    }

});
